package com.maxim.task4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button addition, subtraction, multiplication, division;
    EditText firstNumber, secondNumber;
    TextView result;

    String oper = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumber = (EditText) findViewById(R.id.editText_enter_first_number);

        secondNumber = (EditText) findViewById(R.id.editText_enter_second_number);

        result = (TextView) findViewById(R.id.textView_result);

        addition = (Button) findViewById(R.id.button_addition);
        addition.setOnClickListener(new OperatorListener("+"));

        subtraction = (Button) findViewById(R.id.button_subtraction);
        subtraction.setOnClickListener(new OperatorListener("-"));

        multiplication = (Button) findViewById(R.id.button_multiplication);
        multiplication.setOnClickListener(new OperatorListener("*"));

        division = (Button) findViewById(R.id.button_division);
        division.setOnClickListener(new OperatorListener("/"));
    }

    class OperatorListener implements View.OnClickListener {

        private String oper;

        public OperatorListener(String oper) {
            this.oper = oper;
        }

        @Override
        public void onClick(View v) {
            final String firstNumberStr = firstNumber.getText().toString();
            final float number1 = firstNumberStr.equals("") ? 0 : Float.parseFloat(firstNumberStr);

            final String secondNumberStr = secondNumber.getText().toString();
            final float number2 = secondNumberStr.equals("") ? 0 : Float.parseFloat(secondNumberStr);

            float resultOfCalculations = 0;
            String mainText = number1 + " " + oper + " " + number2 + " = ";
            String text = null;

            switch (oper) {
                case "+":
                    resultOfCalculations = number1 + number2;
                    text = mainText + resultOfCalculations;
                    break;
                case "-":
                    resultOfCalculations = number1 - number2;
                    text = mainText + resultOfCalculations;
                    break;
                case "*":
                    resultOfCalculations = number1 * number2;
                    text = mainText + resultOfCalculations;
                    break;
                case "/":
                    if (number1 == 0 && number2 == 0) {
                        text = "Can not divide 0 by 0";
                    } else if (number2 == 0) {
                        text = "Can not divide by 0";
                    } else {
                        resultOfCalculations = number1 / number2;
                        text = mainText + resultOfCalculations;
                    }
                    break;
                default:
                    // no such operation
            }
            result.setText(text);
        }
    }
}
